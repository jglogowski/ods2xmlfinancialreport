/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.convert;

import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.*;

public enum DataType {
    ODS(
            new Class[]{Date.class},
            new Class[]{BigDecimal.class},
            new Class[]{String.class},
            new Class[]{Boolean.class},
            new Class[]{List.class, Date.class, BigDecimal.class, String.class, Boolean.class},
            new Class[]{byte[].class},
            new Class[]{String.class}),
    XML(
            new Class[]{XMLGregorianCalendar.class},
            new Class[]{BigDecimal.class},
            new Class[]{String.class},
            new Class[]{boolean.class},
            new Class[]{List.class},
            new Class[]{byte[].class},
            new Class[]{Enum.class}),
    GENERIC(
            new Class[]{Date.class},
            new Class[]{Number.class},
            new Class[]{String.class},
            new Class[]{Boolean.class},
            new Class[]{Collection.class},
            new Class[]{byte[].class},
            new Class[]{Enum.class});

    private final Class[] dateClasses;
    private final Class[] numberClasses;
    private final Class[] stringClasses;
    private final Class[] booleanClasses;
    private final Class[] listClasses;
    private final Class[] fileClasses;
    private final Class[] enumClasses;

    DataType(Class[] dateClasses, Class[] numberClasses, Class[] stringClasses, Class booleanClasses[], Class[] listClasses, Class[] fileClasses, Class[] enumClasses) {
        this.dateClasses = dateClasses;
        this.numberClasses = numberClasses;
        this.stringClasses = stringClasses;
        this.booleanClasses = booleanClasses;
        this.listClasses = listClasses;
        this.fileClasses = fileClasses;
        this.enumClasses = enumClasses;
    }

    public enum AbstractType {
        DATE,
        NUMBER,
        STRING,
        BOOLEAN,
        LIST,
        FILE,
        ENUM;
    }

    public Set<Class> getClassesFor(AbstractType type) {
        Class[] classes = {};
        if (type != null) {
            switch (type) {
                case DATE:
                    classes = dateClasses;
                    break;
                case NUMBER:
                    classes = numberClasses;
                    break;
                case STRING:
                    classes = stringClasses;
                    break;
                case BOOLEAN:
                    classes = booleanClasses;
                    break;
                case LIST:
                    classes = listClasses;
                    break;
                case FILE:
                    classes = fileClasses;
                    break;
                case ENUM:
                    classes = enumClasses;
                    break;

            }
        }
        return new HashSet<>(Arrays.asList(classes));
    }

    public AbstractType guessAbstractTypeForClass(Class clazz) {

        for(AbstractType type: AbstractType.values()) {
            Set<Class> classes = getClassesFor(type);
            if(classes.size() == 1 && classes.contains(clazz)) {
                return type;
            }
        }

        return null;
    }

    public Class guessClassForAbstractType(AbstractType abstractType) {
        Set<Class> classes = getClassesFor(abstractType);
        return new ArrayList<>(classes).get(0);
    }
}
