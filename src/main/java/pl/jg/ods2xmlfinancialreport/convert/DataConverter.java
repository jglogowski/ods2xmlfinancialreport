/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.convert;

import javax.xml.datatype.DatatypeConfigurationException;
import java.util.List;

public interface DataConverter {

    Object getConverted(String key, DataType.AbstractType typeHint, Integer listIndex) throws DatatypeConfigurationException;

    List<Object> getConvertedToList(String key, Class listComponentType, Integer listIndex) throws DatatypeConfigurationException;

    boolean isValuePresent(String key, DataType.AbstractType typeHint, Integer listIndex) throws DatatypeConfigurationException;

}
