/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.convert;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.*;

public class Ods2XmlDataConverter implements DataConverter {

    private final Map<String, Object> rawData;


    Ods2XmlDataConverter(Map<String, Object> rawData) {
        this.rawData = rawData;
    }

    @Override
    public Object getConverted(String key, DataType.AbstractType typeHint, Integer listIndex) throws DatatypeConfigurationException {
        Object obj = rawData.getOrDefault(key, null);
        return convert(obj, typeHint, listIndex);
    }


    @Override
    public boolean isValuePresent(String key, DataType.AbstractType typeHint, Integer listIndex) throws DatatypeConfigurationException {
        if (typeHint == null || typeHint == DataType.AbstractType.LIST) {
            return rawData.keySet().stream()
                    .filter(k -> k.startsWith(key) && (k.equals(key) || k.charAt(key.length()) == '.'))
                    .anyMatch(k -> isNotAlwaysConvertedToNull(rawData.get(k), listIndex));
        } else {
            return convert(rawData.getOrDefault(key, null), typeHint, listIndex) != null;
        }
    }

    private boolean isNotAlwaysConvertedToNull(Object obj, Integer listIndex) {
        return Arrays.asList(DataType.AbstractType.values()).stream()
                .anyMatch(abstractType -> {
                    try {
                        return convert(obj, abstractType, listIndex) != null;
                    } catch (DatatypeConfigurationException e) {
                        return false;
                    }
                });
    }

    private Object convert(Object obj, DataType.AbstractType typeHint, Integer listIndex) throws DatatypeConfigurationException {

        if (obj == null) {
            return null;
        }

        if (typeHint == null) {
            return null;
        }

        if (listIndex != null) {
            if (!(obj instanceof List)) {
                return null;
            }

            if (((List) obj).size() <= listIndex) {
                return null;
            }

            obj = ((List) obj).get(listIndex);
        }

        if (!isClassValidForTypeHint(obj, typeHint)) {
            return null;
        }


        switch (typeHint) {
            case DATE:
                String dateString = DateFormat.getDateInstance().format((Date) obj);
                return DatatypeFactory.newInstance().newXMLGregorianCalendar(dateString);
            case ENUM:
            case STRING:
                if (((String) obj).isEmpty()) {
                    return null;
                }
                return obj;
            case NUMBER:
                return ((BigDecimal) obj).setScale(2, BigDecimal.ROUND_HALF_UP);
            case BOOLEAN:
                return obj;
            case FILE:
                return obj;
            case LIST:
                if (obj instanceof List) {
                    if (((List) obj).isEmpty()) {
                        return null;
                    }

                    Class odsComponentType = ((List) obj).get(0).getClass();
                    DataType.AbstractType componentAbstractType = DataType.ODS.guessAbstractTypeForClass(odsComponentType);
                    Class xmlComponentType = DataType.XML.guessClassForAbstractType(componentAbstractType);

                    return convertToList(obj, xmlComponentType, listIndex);
                } else {
                    return convertToList(obj, obj.getClass(), listIndex);
                }
        }

        return null;
    }

    @Override
    public List<Object> getConvertedToList(String key, Class xmlListComponentType, Integer listIndex) throws DatatypeConfigurationException {
        Object obj = rawData.getOrDefault(key, null);
        return convertToList(obj, xmlListComponentType, listIndex);
    }


    private List<Object> convertToList(Object obj, Class xmlListComponentType, Integer listIndex) throws DatatypeConfigurationException {

        if (xmlListComponentType == null) {
            return null;
        }

        DataType.AbstractType listComponentAbstractType = DataType.XML.guessAbstractTypeForClass(xmlListComponentType);

        if (listComponentAbstractType == null) {
            return null;
        }

        if (!isClassValidForTypeHint(obj, DataType.AbstractType.LIST)) {
            return null;
        }


        List<Object> result = new ArrayList<>();

        if (obj instanceof List) {
            if (((List) obj).isEmpty()) {
                return null;
            }

            for (Object component : (List) obj) {
                Object convertedComponent = convert(component, listComponentAbstractType, listIndex);
                if (convertedComponent != null) {
                    result.add(convertedComponent);
                }
            }
        } else {
            Object convertedObj = convert(obj, listComponentAbstractType, listIndex);
            if (convertedObj != null) {
                result.add(convertedObj);
            }
        }

        return result.isEmpty() ? null : result;

    }

    private boolean isClassValidForTypeHint(Object obj, DataType.AbstractType typeHint) {

        if (obj == null) {
            return false;
        }

        Set<Class> expectedClasses = DataType.ODS.getClassesFor(typeHint);


        for (Class expectedClass : expectedClasses) {
            if (expectedClass.isInstance(obj)) {
                return true;
            }
        }

        return false;
    }


}
