/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.convert;

import java.util.Arrays;

public enum SupportedConversions {

    ODS2XML(DataType.ODS, DataType.XML);


    private final DataType fromType;
    private final DataType toType;

    SupportedConversions(DataType fromType, DataType toType) {
        this.fromType = fromType;
        this.toType = toType;
    }

    public static boolean isSupported(DataType fromType, DataType toType) {
        return Arrays.asList(values()).stream()
                .anyMatch(conv -> conv.fromType == fromType && conv.toType == toType);
    }
}
