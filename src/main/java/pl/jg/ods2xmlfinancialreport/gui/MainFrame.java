/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.gui;

import org.apache.commons.io.*;
import pl.jg.ods2xmlfinancialreport.Application;
import pl.jg.ods2xmlfinancialreport.output.xml.RequiredDataNotFoundException;
import pl.jg.ods2xmlfinancialreport.output.xml.XmlElementException;
import pl.jg.ods2xmlfinancialreport.output.xml.XmlValidationFailedException;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainFrame extends JFrame {

    Container rootContentPane;

    private JPanel labelPanel;
    private JPanel fileChoicePanel;
    private JPanel flowControlPanel;
    private JPanel errorPanel;

    private JLabel titleLabel;

    private JTextArea errorTextArea;
    private Font plainFont;

    private JTextField[] filePathFields;
    private JFileChooser[] fileChoosers;
    private JLabel[] fileDescriptionLabels;
    private FileFilter odsFilter;
    private FileFilter xmlFilter;
    private JButton[] fileChoiceButtons;
    private File currentDir;

    private JButton cancelButton;
    private JButton nextButton;
    private JButton showXmlButton;

    private String odsFilePath;
    private List<String> infoFileDescriptions;
    private String[] infoFilePaths;
    private String xmlFilePath;

    private ActionListener fileOpenActionListener;
    private ActionListener fileSaveActionListener;

    private ActionListener actionListenerAfterOds;
    private ActionListener actionListenerAfterInfo;
    private ActionListener actionListenerAfterXml;
    private ActionListener actionListenerAfterValidation;

    private Application application;

    private final String errorHeaderProcessing = "Podczas przetwarzania danych wystąpił błąd";
    private final String errorHeaderValidation = "Sprawdzenie zgodności pliku ze schematem nie powiodło się.";
    private String errorHeader;

    private String errorMsg;

    private SwingWorker<List<String>, Void> dataImportWorker = new SwingWorker<List<String>, Void>() {

        @Override
        protected List<String> doInBackground() throws Exception {
            application.runDataImport(odsFilePath);
            return application.getAttachmentsDescriptions();
        }

        @Override
        public void done() {
            try {

                infoFileDescriptions = get();

                if (infoFileDescriptions == null || infoFileDescriptions.isEmpty()) {
                    // very ugly, but I've got no time :(
                    throw new XmlElementException(new RequiredDataNotFoundException(), "JednostkaInna.DodatkoweInformacjeIObjasnieniaJednstkaInna.DodatkoweInformacjeIObjasnienia.Opis");
                }

            } catch (ExecutionException e) {
                e.printStackTrace();
                Throwable cause = e.getCause();
                errorMsg = cause.toString();
                errorHeader  = errorHeaderProcessing;
                setUpError();
                return;
            } catch (XmlElementException e) {
                errorMsg = e.toString() + " [ścieżka XML: " + e.getXmlPath() + "]";
                errorHeader  = errorHeaderProcessing;
                setUpError();
                return;
            } catch (InterruptedException ignore) {
            }

            setUpInfoFileChoice();
        }
    };

    private SwingWorker<Void, Void> fileImportWorker = new SwingWorker<Void, Void>() {

        @Override
        protected Void doInBackground() throws Exception {
            application.runFileImport(infoFilePaths);
            return null;
        }

        @Override
        public void done() {
            try {
                get();
            } catch (ExecutionException e) {
                e.printStackTrace();
                Throwable cause = e.getCause();
                errorMsg = cause.toString();
                errorHeader  = errorHeaderProcessing;
                setUpError();
                return;

            } catch (InterruptedException ignore) {
            }

            setUpXmlFileChoice();
        }
    };

    private SwingWorker<Void, Void> exportWorker = new SwingWorker<Void, Void>() {

        @Override
        protected Void doInBackground() throws Exception {
            application.runExport(xmlFilePath);
            return null;
        }

        @Override
        public void done() {
            try {
                get();
            } catch (ExecutionException e) {
                e.printStackTrace();
                Throwable cause = e.getCause();
                errorMsg = cause.toString();
                if (cause instanceof XmlElementException) {
                    errorMsg += " [ścieżka XML: " + ((XmlElementException) cause).getXmlPath() + "]";
                }
                errorHeader  = errorHeaderProcessing;
                setUpError();
                return;

            } catch (InterruptedException ignore) {
            }

            setUpValidation();
        }
    };

    private SwingWorker<Boolean, Void> validationWorker = new SwingWorker<Boolean, Void>() {

        @Override
        protected Boolean doInBackground() throws Exception {
            return application.runValidation(xmlFilePath);
        }

        @Override
        public void done() {
            try {
                get();
            } catch (ExecutionException e) {
                e.printStackTrace();
                Throwable cause = e.getCause();
                if(cause instanceof XmlValidationFailedException) {
                    cause = cause.getCause();
                }
                errorMsg = cause.toString();
                errorHeader = errorHeaderValidation;
                setUpError();
                return;

            } catch (InterruptedException ignore) {
            }

            setUpFinish();
        }
    };

    public MainFrame() {
        super("Generowanie e-sprawozdania XML");
        application = new Application();
        setUp();
    }

    private void customizeFileChooser() {
        UIManager.put("FileChooser.lookInLabelText", "Szukaj w");
        UIManager.put("FileChooser.saveInLabelText", "Zapisz w");
        UIManager.put("FileChooser.fileNameLabelText", "Nazwa pliku");
        UIManager.put("FileChooser.homeFolderToolTipText", "Katalog domowy");
        UIManager.put("FileChooser.newFolderToolTipText", "Nowy katalog");
        UIManager.put("FileChooser.listViewButtonToolTipText", "Widok listy");
        UIManager.put("FileChooser.detailsViewButtonToolTipText", "Widok szczegółowy");
        UIManager.put("FileChooser.saveButtonText", "Zapisz");
        UIManager.put("FileChooser.openButtonText", "Otwórz");
        UIManager.put("FileChooser.cancelButtonText", "Anuluj");
        UIManager.put("FileChooser.updateButtonText", "Odśwież");
        UIManager.put("FileChooser.helpButtonText", "Pomoc");
        UIManager.put("FileChooser.saveButtonToolTipText", "Wybranie pliku do zapisania");
        UIManager.put("FileChooser.cancelButtonToolTipText", "Powrót bez wprowadzania zmian");
        UIManager.put("FileChooser.updateButtonToolTipText", "Odświeżenie widoku katalogu");
        UIManager.put("FileChooser.helpButtonToolTipText", "Wyświetla pomoc");
        UIManager.put("FileChooser.filesOfTypeLabelText", "Typ pliku");
        UIManager.put("FileChooser.upFolderToolTipText", "Przejdź do katalogu nadrzędnego");
        UIManager.put("FileChooser.openDialogTitleText","Otwórz");
        UIManager.put("FileChooser.saveDialogTitleText","Zapisz jako");
        UIManager.put("FileChooser.readOnly", Boolean.TRUE);

    }

    private void setUp() {
        setIconImage(new ImageIcon(this.getClass().getResource("ant_64x64.png")).getImage());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocation(300, 300);
        plainFont = new Font("Dialog", Font.PLAIN, 12);

        rootContentPane = getContentPane(); // BorderLayout
        labelPanel = new JPanel();
        labelPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

        titleLabel = new JLabel();
        titleLabel.setFont(plainFont);
        labelPanel.add(titleLabel);

        errorPanel = new JPanel();
        errorPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
        errorTextArea = new JTextArea();
        errorTextArea.setLineWrap(true);
        errorTextArea.setColumns(60);
        errorTextArea.setRows(15);
        errorTextArea.setEditable(false);
        errorPanel.add(errorTextArea);
        errorPanel.setVisible(false);

        fileChoicePanel = new JPanel();
        fileChoicePanel.setLayout(new BoxLayout(fileChoicePanel, BoxLayout.PAGE_AXIS));
        fileChoicePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        customizeFileChooser();
        setUpFileChoicePanel();

        flowControlPanel = new JPanel(); // FlowLayout is default
        flowControlPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        flowControlPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        cancelButton = new JButton("Anuluj");


        nextButton = new JButton("Dalej");
        nextButton.setEnabled(false);

        showXmlButton = new JButton("Pokaż wygenerowany plik");
        showXmlButton.setVisible(false);

        flowControlPanel.add(cancelButton);
        flowControlPanel.add(showXmlButton);
        flowControlPanel.add(nextButton);

        rootContentPane.add(labelPanel, BorderLayout.NORTH);
        rootContentPane.add(fileChoicePanel, BorderLayout.CENTER);
        rootContentPane.add(flowControlPanel, BorderLayout.SOUTH);

        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        setUpOdsFileChoice();

        //Display the window.
        pack();
        setVisible(true);
    }

    private void setUpFileChoicePanel() {
        int count = 1;

        if (infoFileDescriptions != null && !infoFileDescriptions.isEmpty()) {
            count = infoFileDescriptions.size();
            setUpFileDescriptions();
        } else {
            fileDescriptionLabels = null;
        }

        fileChoicePanel.removeAll();

        setUpFilePathFields(count);
        setUpFileChoiceButtons(count);
        setUpFileChoosers(count);


        for (int i = 0; i < count; i++) {
            JPanel singleFileChoicePanel = new JPanel();
            singleFileChoicePanel.setLayout(new FlowLayout(FlowLayout.CENTER));
            if (fileDescriptionLabels != null && fileDescriptionLabels.length == count) {
                singleFileChoicePanel.add(fileDescriptionLabels[i]);
            }
            singleFileChoicePanel.add(filePathFields[i]);
            singleFileChoicePanel.add(fileChoiceButtons[i]);
            fileChoicePanel.add(singleFileChoicePanel);
        }

        pack();
        fileChoicePanel.revalidate();
        fileChoicePanel.repaint();
    }

    private void setUpFilePathFields(int count) {

        filePathFields = new JTextField[count];
        for (int i = 0; i < count; i++) {
            filePathFields[i] = new JTextField();
            ;
            filePathFields[i].setPreferredSize(new Dimension(400, 20));
            filePathFields[i].setFont(plainFont);
            filePathFields[i].setEnabled(false);
            filePathFields[i].getDocument().addDocumentListener(new DocumentListener() {

                private void setNextButtonAccess() {

                    for (JTextField pathField : filePathFields) {
                        if (pathField.getText().isEmpty()) {
                            nextButton.setEnabled(false);
                            return;
                        }
                    }
                    nextButton.setEnabled(true);
                }

                @Override
                public void insertUpdate(DocumentEvent e) {
                    setNextButtonAccess();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    setNextButtonAccess();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    setNextButtonAccess();
                }
            });

        }
    }

    private void setUpFileChoiceButtons(int count) {

        fileOpenActionListener = new FileOpenActionListener();
        fileChoiceButtons = new JButton[count];

        for (int i = 0; i < count; i++) {
            fileChoiceButtons[i] = new JButton("Wybierz...");
            fileChoiceButtons[i].setActionCommand(String.valueOf(i));
            fileChoiceButtons[i].addActionListener(fileOpenActionListener);
        }
    }

    private void setUpFileChoosers(int count) {
        fileChoosers = new JFileChooser[count];
        for (int i = 0; i < count; i++) {
            fileChoosers[i] = new JFileChooser();
        }
    }

    private void setUpFileDescriptions() {
        if (infoFileDescriptions == null || infoFileDescriptions.isEmpty()) {
            return;
        }

        int count = infoFileDescriptions.size();

        fileDescriptionLabels = new JLabel[count];

        for (int i = 0; i < count; i++) {
            JLabel descLabel = new JLabel();
            descLabel.setPreferredSize(new Dimension(200, 20));
            descLabel.setText(infoFileDescriptions.get(i));
            descLabel.setFont(plainFont);
            fileDescriptionLabels[i] = descLabel;
        }
    }


    private void setUpOdsFileChoice() {
        titleLabel.setText("Wybierz plik sprawozdania finansowego w formacie Open Office (*.ods): ");

        odsFilter = new OdsFileFilter();

        for (JFileChooser fileChooser : fileChoosers) {
            fileChooser.setFileFilter(odsFilter);
        }

        actionListenerAfterOds = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                odsFilePath = filePathFields[0].getText();
                setUpProcessingDataImport();
            }
        };
        nextButton.addActionListener(actionListenerAfterOds);

    }

    private void setUpProcessingDataImport() {
        nextButton.setEnabled(false);
        for (JButton fileChoiceButton : fileChoiceButtons) {
            fileChoiceButton.setEnabled(false);
        }
        titleLabel.setText("Wczytywanie danych z pliku ODS...");
        dataImportWorker.execute();
    }

    private void setUpInfoFileChoice() {

        titleLabel.setText("Wybierz pliki załączników z informacją dodatkową: ");

        setUpFileChoicePanel();

        for (JFileChooser fileChooser : fileChoosers) {
            fileChooser.resetChoosableFileFilters();
            fileChooser.setSelectedFile(new File(""));
        }
        nextButton.setEnabled(false);

        nextButton.removeActionListener(actionListenerAfterOds);
        actionListenerAfterInfo = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                infoFilePaths = Arrays.stream(filePathFields)
                        .map(fpf -> fpf.getText())
                        .toArray(String[]::new);

                setUpProcessingFileImport();
            }
        };
        nextButton.addActionListener(actionListenerAfterInfo);
    }

    private void setUpProcessingFileImport() {
        nextButton.setEnabled(false);
        for (JButton fileChoiceButton : fileChoiceButtons) {
            fileChoiceButton.setEnabled(false);
        }
        titleLabel.setText("Trwa załączanie plików...");
        fileImportWorker.execute();
    }

    private void setUpXmlFileChoice() {

        titleLabel.setText("Podaj nazwę wynikowego pliku XML: ");

        fileDescriptionLabels = null;
        infoFileDescriptions = null;

        setUpFileChoicePanel();

        xmlFilter = new XmlFileFilter();

        xmlFilePath = FilenameUtils.getBaseName(odsFilePath) + ".xml";

        for (JFileChooser fileChooser : fileChoosers) {
            fileChooser.resetChoosableFileFilters();
            fileChooser.setSelectedFile(new File(xmlFilePath));
            fileChooser.setFileFilter(xmlFilter);
        }

        fileSaveActionListener = new FileSaveActionListener();

        for (JButton fileChoiceButton : fileChoiceButtons) {
            fileChoiceButton.removeActionListener(fileOpenActionListener);
            fileChoiceButton.addActionListener(fileSaveActionListener);
        }

        nextButton.setEnabled(false);

        nextButton.removeActionListener(actionListenerAfterInfo);
        actionListenerAfterXml = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                xmlFilePath = filePathFields[0].getText();
                setUpProcessingExport();
            }
        };
        nextButton.addActionListener(actionListenerAfterXml);
    }


    private void setUpProcessingExport() {
        nextButton.setEnabled(false);
        for (JButton fileChoiceButton : fileChoiceButtons) {
            fileChoiceButton.setEnabled(false);
        }
        titleLabel.setText("Proszę czekać, trwa przetwarzanie danych...");
        exportWorker.execute();
    }

    private void setUpValidation() {
        titleLabel.setText("Plik XML został utworzony. Kliknij \"Dalej\", aby rozpocząć sprawdzenie poprawności.");
        fileChoicePanel.setVisible(false);
        cancelButton.setVisible(false);


        nextButton.removeActionListener(actionListenerAfterXml);

        actionListenerAfterValidation = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setUpProcessingValidation();
            }
        };

        nextButton.addActionListener(actionListenerAfterValidation);

        nextButton.setEnabled(true);

        if (Desktop.isDesktopSupported()) {

            showXmlButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Desktop desktop = Desktop.getDesktop();
                    try {
                        desktop.open(new File(xmlFilePath));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });
            showXmlButton.setVisible(true);
        }
    }

    private void setUpProcessingValidation() {
        nextButton.setEnabled(false);
        showXmlButton.setEnabled(false);
        titleLabel.setText("Proszę czekać, trwa analiza pliku XML...");
        validationWorker.execute();
    }

    private void setUpFinish() {
        titleLabel.setForeground(new Color(102,204,0));
        titleLabel.setText("Plik XML jest zgodny ze schematem XSD.");
        fileChoicePanel.setVisible(false);
        nextButton.setText("Zakończ");
        nextButton.removeActionListener(actionListenerAfterValidation);
        nextButton.addActionListener(e -> System.exit(0));
        nextButton.setEnabled(true);
        showXmlButton.setEnabled(true);


    }

    private void setUpError() {
        titleLabel.setText(errorHeader);
        titleLabel.setForeground(Color.RED);
        rootContentPane.remove(fileChoicePanel);
        cancelButton.setVisible(false);
        errorTextArea.setLineWrap(true);
        errorTextArea.setText(errorMsg);
        rootContentPane.add(errorPanel, BorderLayout.CENTER);
        errorPanel.setVisible(true);

        nextButton.setText("Zakończ");
        Arrays.stream(nextButton.getActionListeners())
                .forEach(nextButton::removeActionListener);
        nextButton.addActionListener(e -> System.exit(0));
        nextButton.setEnabled(true);
        showXmlButton.setEnabled(true);
        pack();

    }

    private class FileOpenActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            int fileChoiceIndex = Integer.valueOf(e.getActionCommand());

            if (currentDir != null) {
                fileChoosers[fileChoiceIndex].setCurrentDirectory(currentDir);
            }

            int returnVal = fileChoosers[fileChoiceIndex].showOpenDialog(MainFrame.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChoosers[fileChoiceIndex].getSelectedFile();
                filePathFields[fileChoiceIndex].setText(selectedFile.getAbsolutePath());
                currentDir = selectedFile.getParentFile();
            }
        }
    }

    private class FileSaveActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            int fileChoiceIndex = Integer.valueOf(e.getActionCommand());

            if (currentDir != null) {
                fileChoosers[fileChoiceIndex].setCurrentDirectory(currentDir);
            }

            int returnVal = fileChoosers[fileChoiceIndex].showSaveDialog(MainFrame.this);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChoosers[fileChoiceIndex].getSelectedFile();
                filePathFields[fileChoiceIndex].setText(selectedFile.getAbsolutePath());
                currentDir = selectedFile.getParentFile();
            }
        }
    }
}
