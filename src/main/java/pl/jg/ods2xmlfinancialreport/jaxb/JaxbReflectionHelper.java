/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.jaxb;

import javax.xml.bind.annotation.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class JaxbReflectionHelper {

    Object jaxbObjectFactory;

    public JaxbReflectionHelper(Object jaxbObjectFactory) {
        this.jaxbObjectFactory = jaxbObjectFactory;
    }

    private Set<Class> getXmlAnnotationClasses() {
        return new HashSet<>(
                Arrays.asList(
                        XmlValue.class,
                        XmlAttribute.class,
                        XmlElement.class,
                        XmlElements.class,
                        XmlElementRefs.class
                ));
    }

    private boolean hasXmlAnnotation(Field field) {
        return getXmlAnnotationClassForField(field) != null;
    }

    private Class getXmlAnnotationClassForField(Field field) {
        for (Class annotationClass : getXmlAnnotationClasses()) {
            if (field.isAnnotationPresent(annotationClass)) {
                return annotationClass;
            }
        }

        return null;
    }

    public Set<Field> getAllFieldsWithXmlAnnotation(Class clazz) {
        Set<Field> fields = new HashSet<>();
        Class ancestorClass = clazz;
        while (!ancestorClass.equals(Object.class)) {
            fields.addAll(Arrays.asList(ancestorClass.getDeclaredFields()).stream()
                    .filter(field -> hasXmlAnnotation(field))
                    .collect(Collectors.toSet()));

            ancestorClass = ancestorClass.getSuperclass();
        }

        return fields;
    }

    public String getJaxbCreateMethodNameForClass(Class clazz) {

        StringBuilder sb = new StringBuilder();

        String packageName = clazz.getPackage().getName();
        String className = clazz.getName();

        sb
                .append(className)
                .delete(0, packageName.length() + 1)
                .insert(0, "create");

        return Pattern.compile("\\$").matcher(sb).replaceAll("");
    }

    public Method getJaxbCreateMethodForClass(Class clazz) throws NoSuchMethodException {
        String methodName = getJaxbCreateMethodNameForClass(clazz);
        return jaxbObjectFactory.getClass().getMethod(methodName);
    }

    public boolean isJaxbGenerated(Class clazz) {
        if (clazz.isPrimitive() || clazz.isArray()) {
            return false;
        }
        return clazz.getPackage().equals(jaxbObjectFactory.getClass().getPackage());
    }

    public static boolean isList(Class clazz) {
        return clazz.equals(List.class);
    }

    public static boolean isChoice(Field field) {
        return field.isAnnotationPresent(XmlElements.class);
    }

    public static boolean isRefChoice(Field field) {
        return field.isAnnotationPresent(XmlElementRefs.class);
    }

    public static Class getChosenElementTypeByName(Field field, String elementName) {

        XmlElement[] options = field.getAnnotation(XmlElements.class).value();

        for (XmlElement option : options) {
            if (option.name().equals(elementName)) {
                return option.type();
            }
        }

        return null;
    }


    public static Class getListFieldComponentClass(Field listField) throws ClassNotFoundException {
        if (!isList(listField.getType())) {
            return null;
        }
        String genericTypeName = listField.getGenericType().getTypeName();
        int firstAngleBracketPosition = genericTypeName.indexOf("<");
        int lastAngleBracketPosition = genericTypeName.lastIndexOf(">");
        genericTypeName = genericTypeName.substring(firstAngleBracketPosition + 1, lastAngleBracketPosition);
        return Class.forName(genericTypeName);
    }

    public Boolean isRequiredBySchema(Field field) {

        Class annotationClass = getXmlAnnotationClassForField(field);

        if (annotationClass == null) {
            return null;
        }

        Annotation annotation = field.getAnnotation(annotationClass);

        if (annotation instanceof XmlElement) {
            return ((XmlElement) annotation).required();
        } else if (annotation instanceof XmlAttribute) {
            return ((XmlAttribute) annotation).required();
        } else if (annotation instanceof XmlValue) {
            return true;
        } else if (annotation instanceof XmlElements || annotation instanceof XmlElementRefs) {
            return true;
        }

        return null;


    }

    public Object createObjectForJaxbClass(Class clazz) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method createMethod = getJaxbCreateMethodForClass(clazz);
        return createMethod.invoke(jaxbObjectFactory);
    }

    public Object createObjectOfJaxbElementType(Class parentClass, String fieldXmlName, String value) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String methodName = "create" + parentClass.getSimpleName() + fieldXmlName;
        Method createMethod = jaxbObjectFactory.getClass().getMethod(methodName, String.class);
        return createMethod.invoke(jaxbObjectFactory, value);
    }

    private String getXmlNameForField(Field field) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {

        Class annotationClass = getXmlAnnotationClassForField(field);

        if (annotationClass == null) {
            return null;
        }

        Annotation annotation = field.getAnnotation(annotationClass);

        if (annotation instanceof XmlElement) {
            return ((XmlElement) annotation).name();
        } else if (annotation instanceof XmlAttribute) {
            return ((XmlAttribute) annotation).name();
        } else if (annotation instanceof XmlValue) {
            return "Value";
        } else if (annotation instanceof XmlElements || annotation instanceof XmlElementRefs) {
            return getXmlChoiceFieldName(annotation);
        }

        return null;
    }

    private String getXmlChoiceFieldName(Annotation xmlChoiceAnnotation) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        if (!(xmlChoiceAnnotation instanceof XmlElements || xmlChoiceAnnotation instanceof XmlElementRefs)) {
            return null;
        }

        Class annotationClass = xmlChoiceAnnotation.getClass();
        Method valueMethod = annotationClass.getMethod("value");
        Object[] options = (Object[]) valueMethod.invoke(xmlChoiceAnnotation);

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < options.length; i++) {
            Method nameMethod = options[i].getClass().getMethod("name");
            sb.append(capitalize((String) nameMethod.invoke(options[i])));
            if (i < options.length - 1) {
                sb.append("Or");
            }
        }

        return sb.toString();


    }


    public String getFieldPartForCalls(String fieldXmlElementName) {
        return capitalize(fieldXmlElementName.replaceAll("_", ""));
    }


    public String getFieldPartForCalls(Field field) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        return getFieldPartForCalls(getXmlNameForField(field));
    }

    private String capitalize(String str) {
        if (str == null) {
            return null;
        }

        if (str.isEmpty()) {
            return str;
        }

        String firstLetterCapitalized = str.substring(0, 1).toUpperCase();

        return firstLetterCapitalized + (str.length() == 1 ? "" : str.substring(1));
    }

    public JaxbFieldType getJaxbFieldType(Field field) {

        Class fieldType = field.getType();

        if (fieldType.isEnum()) {
            return JaxbFieldType.ENUM;
        } else if (isJaxbGenerated(fieldType)) {
            return JaxbFieldType.JAXB;
        } else if (isList(fieldType)) {
            return JaxbFieldType.LIST;
        } else if (isChoice(field)) {
            return JaxbFieldType.CHOICE;
        } else if (isRefChoice(field)) {
            return JaxbFieldType.REFCHOICE;
        } else {
            return JaxbFieldType.SIMPLE;
        }
    }

}
