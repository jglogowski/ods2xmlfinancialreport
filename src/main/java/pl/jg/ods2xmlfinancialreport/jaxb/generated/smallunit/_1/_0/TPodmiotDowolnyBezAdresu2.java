//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.13 at 10:19:11 PM CET 
//


package pl.jg.ods2xmlfinancialreport.jaxb.generated.smallunit._1._0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * Skr�cony zestaw danych o osobie fizycznej lub niefizycznej z identyfikatorem NIP
 * 
 * <p>Java class for TPodmiotDowolnyBezAdresu2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TPodmiotDowolnyBezAdresu2">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="OsobaFizyczna" type="{http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/}TIdentyfikatorOsobyFizycznej2"/>
 *         &lt;element name="OsobaNiefizyczna" type="{http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/}TIdentyfikatorOsobyNiefizycznej"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TPodmiotDowolnyBezAdresu2", namespace = "http://crd.gov.pl/xml/schematy/dziedzinowe/mf/2016/01/25/eD/DefinicjeTypy/", propOrder = {
    "osobaFizycznaOrOsobaNiefizyczna"
})
public class TPodmiotDowolnyBezAdresu2 {

    @XmlElements({
        @XmlElement(name = "OsobaFizyczna", type = TIdentyfikatorOsobyFizycznej2 .class),
        @XmlElement(name = "OsobaNiefizyczna", type = TIdentyfikatorOsobyNiefizycznej.class)
    })
    protected Object osobaFizycznaOrOsobaNiefizyczna;

    /**
     * Gets the value of the osobaFizycznaOrOsobaNiefizyczna property.
     * 
     * @return
     *     possible object is
     *     {@link TIdentyfikatorOsobyFizycznej2 }
     *     {@link TIdentyfikatorOsobyNiefizycznej }
     *     
     */
    public Object getOsobaFizycznaOrOsobaNiefizyczna() {
        return osobaFizycznaOrOsobaNiefizyczna;
    }

    /**
     * Sets the value of the osobaFizycznaOrOsobaNiefizyczna property.
     * 
     * @param value
     *     allowed object is
     *     {@link TIdentyfikatorOsobyFizycznej2 }
     *     {@link TIdentyfikatorOsobyNiefizycznej }
     *     
     */
    public void setOsobaFizycznaOrOsobaNiefizyczna(Object value) {
        this.osobaFizycznaOrOsobaNiefizyczna = value;
    }

}
