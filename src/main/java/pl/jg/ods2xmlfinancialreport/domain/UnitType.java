/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.domain;

import javax.xml.bind.annotation.XmlRootElement;

public enum UnitType {
    MICRO("Jednostka mikro",
            "pl.jg.ods2xmlfinancialreport.jaxb.generated.microunit",
            new Version[]{Version.V_1_0},
            new String[]{"JednostkaMikro"},
            new String[]{"JednostkaMikro.DodatkoweInformacjeIObjasnieniaJednstkaInna.DodatkoweInformacjeIObjasnienia.Plik.Zawartosc"},
            new String[]{"JednostkaMikro.DodatkoweInformacjeIObjasnieniaJednstkaInna.DodatkoweInformacjeIObjasnienia.Plik.Nazwa"},
            new String[]{"JednostkaMikro.DodatkoweInformacjeIObjasnieniaJednstkaInna.DodatkoweInformacjeIObjasnienia.Opis"},
            new String[]{""}),
    SMALL("Jednostka mała",
            "pl.jg.ods2xmlfinancialreport.jaxb.generated.smallunit",
            new Version[]{Version.V_1_0},
            new String[]{"JednostkaMala"},
            new String[]{"JednostkaMala.DodatkoweInformacjeIObjasnieniaJednstkaInna.DodatkoweInformacjeIObjasnienia.Plik.Zawartosc"},
            new String[]{"JednostkaMala.DodatkoweInformacjeIObjasnieniaJednstkaInna.DodatkoweInformacjeIObjasnienia.Plik.Nazwa"},
            new String[]{"JednostkaMala.DodatkoweInformacjeIObjasnieniaJednstkaInna.DodatkoweInformacjeIObjasnienia.Opis"},
            new String[]{""}),
    OTHER("Jednostka inna",
            "pl.jg.ods2xmlfinancialreport.jaxb.generated.otherunit",
            new Version[]{Version.V_1_0},
            new String[]{"JednostkaInna"},
            new String[]{"JednostkaInna.DodatkoweInformacjeIObjasnieniaJednstkaInna.DodatkoweInformacjeIObjasnienia.Plik.Zawartosc"},
            new String[]{"JednostkaInna.DodatkoweInformacjeIObjasnieniaJednstkaInna.DodatkoweInformacjeIObjasnienia.Plik.Nazwa"},
            new String[]{"JednostkaInna.DodatkoweInformacjeIObjasnieniaJednstkaInna.DodatkoweInformacjeIObjasnienia.Opis"},
            new String[]{"http://www.mf.gov.pl/documents/764034/6464789/JednostkaInnaWZlotych(1)_v1-0.xsd"});

    public enum Version {

        V_1_0("1.0");

        private final String versionString;

        Version(String versionString) {
            this.versionString = versionString;
        }

        @Override
        public String toString() {
            return versionString;
        }
    }

    private String fullName;
    private String packagePrefix;
    private Version[] versions;
    private String[] xmlRootElementsForVersions;
    private String[] attachedFileContentKeys;
    private String[] attachedFileNameKeys;
    private String[] attachedFileDescriptionKeys;
    private String[] schemaURLs;

    UnitType(String fullName,
             String packagePrefix,
             Version[] versions,
             String[] xmlRootElementsForVersions,
             String[] attachedFileContentKeys,
             String[] attachedFileNameKeys,
             String[] attachedFileDescriptionKeys,
             String[] schemaURLs) {
        this.fullName = fullName;
        this.packagePrefix = packagePrefix;
        this.versions = versions;
        this.xmlRootElementsForVersions = xmlRootElementsForVersions;
        this.attachedFileContentKeys = attachedFileContentKeys;
        this.attachedFileNameKeys = attachedFileNameKeys;
        this.attachedFileDescriptionKeys = attachedFileDescriptionKeys;
        this.schemaURLs = schemaURLs;
    }

    public String getFullName() {
        return fullName;
    }

    public Version[] getVersions() {
        return versions;
    }

    private int getVersionIndex(String version) {

        int index = -1;

        for (int i = 0; i < versions.length; i++) {
            if (version.equals(versions[i].toString())) {
                index = i;
                break;
            }
        }
        return index;
    }

    public String getPackageNameForVersion(Version version) {
        return getPackageNameForVersion(version.toString());
    }

    public String getPackageNameForVersion(String version) {

        int versionIndex = getVersionIndex(version);

        if (versionIndex == -1) {
            return null;
        }

        String[] versionParts = version.split("\\.");

        StringBuilder sb = new StringBuilder(packagePrefix);
        sb.append(".");

        for (int i = 0; i < versionParts.length; i++) {
            sb
                    .append("_")
                    .append(versionParts[i])
                    .append(".");
        }

        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    public String getXmlRootElementNameForVersion(String version) {
        int versionIndex = getVersionIndex(version);

        if (versionIndex == -1) {
            return null;
        }

        return xmlRootElementsForVersions[versionIndex];
    }

    public String getXmlRootElementNameForVersion(Version version) {
        return getXmlRootElementNameForVersion(version.toString());
    }

    public String getAttachedFileContentKeyForVersion(String version) {
        int versionIndex = getVersionIndex(version);

        if (versionIndex == -1) {
            return null;
        }

        return attachedFileContentKeys[versionIndex];
    }

    public String getAttachedFileContentKeyForVersion(Version version) {
        return getAttachedFileContentKeyForVersion(version.toString());
    }

    public String getAttachedFileNameKeyForVersion(String version) {
        int versionIndex = getVersionIndex(version);

        if (versionIndex == -1) {
            return null;
        }

        return attachedFileNameKeys[versionIndex];
    }

    public String getAttachedFileNameKeyForVersion(Version version) {
        return getAttachedFileNameKeyForVersion(version.toString());
    }

    public String getAttachedFileDescriptionKeyForVersion(String version) {
        int versionIndex = getVersionIndex(version);

        if (versionIndex == -1) {
            return null;
        }

        return attachedFileDescriptionKeys[versionIndex];
    }

    public String getAttachedFileDescriptionKeyForVersion(Version version) {
        return getAttachedFileDescriptionKeyForVersion(version.toString());
    }

    public boolean hasVersion(String version) {
        return getVersionIndex(version) != -1;
    }

    public boolean hasVersion(Version version) {
        return hasVersion(version.toString());
    }

    public String getNamespaceNameForVersion(Version version) throws ClassNotFoundException {
        String rootElementClassName = getPackageNameForVersion(version) + "." + getXmlRootElementNameForVersion(version);
        Class rootElementClass = Class.forName(rootElementClassName);
        return ((XmlRootElement) rootElementClass.getAnnotation(XmlRootElement.class)).namespace();
    }

    public String getSchemaURLForVersion(Version version) {
        return getSchemaURLForVersion(version.toString());
    }

    public String getSchemaURLForVersion(String version) {
        int versionIndex = getVersionIndex(version);

        if (versionIndex == -1) {
            return null;
        }

        return schemaURLs[versionIndex];
    }

    public String getSchemaLocationForVersion(Version version) throws ClassNotFoundException {
        return getNamespaceNameForVersion(version) + " " + getSchemaURLForVersion(version);
    }

}
