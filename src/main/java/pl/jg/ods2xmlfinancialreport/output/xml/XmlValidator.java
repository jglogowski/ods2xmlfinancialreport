/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.output.xml;

import org.xml.sax.SAXException;
import pl.jg.ods2xmlfinancialreport.domain.UnitType;
import pl.jg.ods2xmlfinancialreport.output.Validator;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class XmlValidator implements Validator {
    private UnitType unitType;
    private UnitType.Version version;
    private Source xmlSource;
    private URL schemaURL;
    private SchemaFactory schemaFactory;



    public XmlValidator(File xmlFile, UnitType unitType, UnitType.Version version) throws MalformedURLException {
        this.xmlSource = new StreamSource(xmlFile);
        this.unitType = unitType;
        this.version = version;
        this.schemaURL = new URL(this.unitType.getSchemaURLForVersion(this.version));
        //this.schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        this.schemaFactory = javax.xml.validation.SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI,"com.sun.org.apache.xerces.internal.jaxp.validation.XMLSchemaFactory", this.getClass().getClassLoader());
    }



    @Override
    public boolean validate() {
        try {
            Schema schema = schemaFactory.newSchema(schemaURL);
            //Schema schema = schemaFactory.newSchema(new File("/home/jarcik/IdeaProjects/ods2xmlfinancialreport/resources/xsd/JednostkaInnaWZlotych(1)_v1-0.xsd"));
            javax.xml.validation.Validator validator = schema.newValidator();
            validator.validate(xmlSource);
        } catch (SAXException e) {
            e.printStackTrace();
            throw new XmlValidationFailedException(e);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return true;
    }
}
