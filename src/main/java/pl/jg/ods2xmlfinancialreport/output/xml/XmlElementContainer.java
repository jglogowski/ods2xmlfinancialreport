/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.output.xml;

import pl.jg.ods2xmlfinancialreport.convert.DataConverter;
import pl.jg.ods2xmlfinancialreport.convert.DataType;
import pl.jg.ods2xmlfinancialreport.jaxb.JaxbFieldType;
import pl.jg.ods2xmlfinancialreport.jaxb.JaxbReflectionHelper;

import javax.xml.datatype.DatatypeConfigurationException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.List;

public class XmlElementContainer {
    private Object jaxbObjectFactory;
    private Object jaxbGeneratedObject;
    private String xmlElementPath;
    private DataConverter dataConverter;
    private JaxbReflectionHelper jaxbReflectionHelper;
    private Package jaxbGeneratedPackage;
    private Integer listIndex;

    public XmlElementContainer(Object jaxbObjectFactory, Object jaxbGeneratedObject, String xmlElementPath, DataConverter dataConverter) {
        this(jaxbObjectFactory, jaxbGeneratedObject, xmlElementPath, dataConverter, null);
    }

    public XmlElementContainer(Object jaxbObjectFactory, Object jaxbGeneratedObject, String xmlElementPath, DataConverter dataConverter, Integer listIndex) {
        this.jaxbObjectFactory = jaxbObjectFactory;
        this.jaxbGeneratedObject = jaxbGeneratedObject;
        this.xmlElementPath = xmlElementPath;
        this.dataConverter = dataConverter;
        this.jaxbReflectionHelper = new JaxbReflectionHelper(jaxbObjectFactory);
        this.jaxbGeneratedPackage = jaxbObjectFactory.getClass().getPackage();
        this.listIndex = listIndex;
    }


    public Object getJaxbGeneratedObject() {
        return jaxbGeneratedObject;
    }

    public void createChildren() {
        Set<Field> childFields = jaxbReflectionHelper.getAllFieldsWithXmlAnnotation(jaxbGeneratedObject.getClass());
        childFields.parallelStream().forEachOrdered(this::createChild);
    }


    private void createChild(Field field) throws XmlElementException {

        String fieldXmlPath = "";

        try {

            fieldXmlPath = getXmlPathForField(field);

            JaxbFieldType jaxbFieldType = jaxbReflectionHelper.getJaxbFieldType(field);
            Class fieldType = field.getType();


/*            boolean fieldIsRequired = jaxbReflectionHelper.isRequiredBySchema(field);

            if (!isDataPresent(fieldXmlPath, jaxbFieldType, fieldType)) {
                if (fieldIsRequired) {
                    throw new RequiredDataNotFoundException(fieldXmlPath);
                } else {
                    return;
                }
            } else if (!fieldIsRequired) {
                Boolean forceOption = (Boolean) dataConverter.getConverted(fieldXmlPath + ".FORCE", DataType.AbstractType.BOOLEAN, listIndex);
                if (forceOption != null && !forceOption) {
                    return;
                }
            }*/

            Boolean forceOption = (Boolean) dataConverter.getConverted(fieldXmlPath + ".FORCE", DataType.AbstractType.BOOLEAN, listIndex);
            boolean dataPresent = isDataPresent(fieldXmlPath, jaxbFieldType, fieldType);


            if (jaxbReflectionHelper.isRequiredBySchema(field)) {
                if (!dataPresent) {
                    throw new RequiredDataNotFoundException();
                }
            } else if (forceOption != null) {
                if (forceOption) {
                    if (!dataPresent) {
                        throw new RequiredDataNotFoundException();
                    }
                } else {
                    return;
                }
            } else if (!dataPresent) {
                return;
            }


            switch (jaxbFieldType) {
                case ENUM:
                    createEnumChild(field, fieldXmlPath);
                    break;
                case JAXB:
                    createJaxbGeneratedChild(field, fieldXmlPath);
                    break;
                case LIST:
                    createListChild(field, fieldXmlPath);
                    break;
                case CHOICE:
                    createChoiceChild(field, fieldXmlPath);
                    break;
                case SIMPLE:
                    createSimpleChild(field, fieldXmlPath);
                    break;
                case REFCHOICE:
                    createRefChoiceChild(field, fieldXmlPath);
                    break;
            }


        } catch (Exception e) {

            if (e instanceof XmlElementException) {
                throw (XmlElementException) e;
            }

            throw new XmlElementException(e, fieldXmlPath);

        }
    }

    private boolean isDataPresent(String fieldXmlPath, JaxbFieldType jaxbFieldType, Class fieldType) throws DatatypeConfigurationException {
        if (jaxbFieldType == JaxbFieldType.JAXB || jaxbFieldType == JaxbFieldType.CHOICE || jaxbFieldType == JaxbFieldType.REFCHOICE) {
            return dataConverter.isValuePresent(fieldXmlPath, null, listIndex);
        } else if (jaxbFieldType == JaxbFieldType.LIST) {
            return dataConverter.isValuePresent(fieldXmlPath, DataType.AbstractType.LIST, listIndex);
        } else {
            return dataConverter.isValuePresent(fieldXmlPath, DataType.XML.guessAbstractTypeForClass(fieldType), listIndex);
        }
    }

    private void createEnumChild(Field field, String fieldXmlPath) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, DatatypeConfigurationException {

        Object data = dataConverter.getConverted(fieldXmlPath, DataType.AbstractType.ENUM, listIndex);
        Class fieldType = field.getType();
        Method conversionToEnumMethod = fieldType.getMethod("fromValue", String.class);
        Object enumValue = fieldType.cast(conversionToEnumMethod.invoke(fieldType, data.toString()));
        Method setMethod = getSetMethodForField(field);
        setMethod.invoke(jaxbGeneratedObject, enumValue);
    }

    private void createJaxbGeneratedChild(Field field, String fieldXmlPath) throws Exception {
        Class fieldType = field.getType();
        Object childJaxbGeneratedObject = jaxbReflectionHelper.createObjectForJaxbClass(fieldType);
        XmlElementContainer child = new XmlElementContainer(jaxbObjectFactory, childJaxbGeneratedObject, fieldXmlPath, dataConverter, listIndex);
        child.createChildren();
        Method setMethod = getSetMethodForField(field);
        setMethod.invoke(jaxbGeneratedObject, childJaxbGeneratedObject);
    }

    private void createSimpleChild(Field field, String fieldXmlPath) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, DatatypeConfigurationException {
        Class fieldType = field.getType();
        Object data = dataConverter.getConverted(fieldXmlPath, DataType.XML.guessAbstractTypeForClass(fieldType), listIndex);
        Method setMethod = getSetMethodForField(field);
        setMethod.invoke(jaxbGeneratedObject, data);
    }

    private void createChoiceChild(Field field, String fieldXmlPath) throws Exception {

        Object choice = dataConverter.getConverted(fieldXmlPath, DataType.AbstractType.STRING, listIndex);
        String chosenName = choice.toString();
        Class chosenType = jaxbReflectionHelper.getChosenElementTypeByName(field, chosenName);
        String chosenObjectXmlPath = getXmlPathForField(chosenName);

        Object chosenObject = null;

        if (chosenType.isEnum()) {
            Object data = dataConverter.getConverted(chosenObjectXmlPath, DataType.AbstractType.ENUM, listIndex);
            Method conversionMethod = chosenType.getMethod("fromValue", String.class);
            chosenObject = chosenType.cast(conversionMethod.invoke(chosenType, data.toString()));
        } else if (jaxbReflectionHelper.isJaxbGenerated(chosenType)) {
            chosenObject = jaxbReflectionHelper.createObjectForJaxbClass(chosenType);
            XmlElementContainer chosenObjectContainer = new XmlElementContainer(jaxbObjectFactory, chosenObject, chosenObjectXmlPath, dataConverter, listIndex);
            chosenObjectContainer.createChildren();
        } else {
            chosenObject = dataConverter.getConverted(chosenObjectXmlPath, DataType.XML.guessAbstractTypeForClass(chosenType), listIndex);
        }

        Method setMethod = getSetMethodForField(field);
        setMethod.invoke(jaxbGeneratedObject, chosenObject);
    }

    private void createRefChoiceChild(Field field, String fieldXmlPath) throws Exception {
        Object choice = dataConverter.getConverted(fieldXmlPath, DataType.AbstractType.STRING, listIndex);
        String chosenName = choice.toString();
        String chosenObjectXmlPath = getXmlPathForField(chosenName);
        String chosenValue = (String) dataConverter.getConverted(chosenObjectXmlPath, DataType.AbstractType.STRING, listIndex);
        Object chosenObject = jaxbReflectionHelper.createObjectOfJaxbElementType(jaxbGeneratedObject.getClass(), chosenName, chosenValue);
        Method setMethod = getSetMethodForField(field);
        setMethod.invoke(jaxbGeneratedObject, chosenObject);
    }

    private void createListChild(Field field, String fieldXmlPath) throws Exception {

        Class componentType = jaxbReflectionHelper.getListFieldComponentClass(field);
        boolean componentsAreJaxbGenerated = jaxbReflectionHelper.isJaxbGenerated(componentType);
        Class dataListComponentType = componentsAreJaxbGenerated ? String.class : componentType;

        List dataList = dataConverter.getConvertedToList(fieldXmlPath, dataListComponentType, listIndex);

        if (dataList == null) {
            return;
        }

        Method getMethod = getGetMethodForField(field);
        List jaxbList = (List) getMethod.invoke(jaxbGeneratedObject);

        for (int i = 0; i < dataList.size(); i++) {
            Integer componentListIndex = i + (listIndex == null ? 0 : listIndex);
            if (componentsAreJaxbGenerated) {
                Object componentJaxbObject = jaxbReflectionHelper.createObjectForJaxbClass(componentType);
                XmlElementContainer componentContainer = new XmlElementContainer(jaxbObjectFactory, componentJaxbObject, fieldXmlPath, dataConverter, componentListIndex);
                componentContainer.createChildren();
                jaxbList.add(componentJaxbObject);
            } else {
                Object data = dataList.get(componentListIndex);
                jaxbList.add(data);
            }
        }
    }

    private String getXmlPathForField(Field field) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        return xmlElementPath + "." + jaxbReflectionHelper.getFieldPartForCalls(field);
    }

    private String getXmlPathForField(String fieldXmlElementName) {
        return xmlElementPath + "." + jaxbReflectionHelper.getFieldPartForCalls(fieldXmlElementName);
    }

    private Method getSetMethodForField(Field field) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String fieldPart = jaxbReflectionHelper.getFieldPartForCalls(field);
        return jaxbGeneratedObject.getClass().getMethod("set" + fieldPart, field.getType());
    }

    private Method getGetMethodForField(Field field) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String fieldPart = jaxbReflectionHelper.getFieldPartForCalls(field);
        return jaxbGeneratedObject.getClass().getMethod("get" + fieldPart);
    }

    public Package getJaxbGeneratedPackage() {
        return jaxbGeneratedPackage;
    }
}
