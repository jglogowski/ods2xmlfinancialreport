/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.output.xml;

import pl.jg.ods2xmlfinancialreport.convert.DataConverter;
import pl.jg.ods2xmlfinancialreport.domain.NoSuchVersionException;
import pl.jg.ods2xmlfinancialreport.domain.UnitType;
import pl.jg.ods2xmlfinancialreport.jaxb.JaxbReflectionHelper;

import java.lang.reflect.InvocationTargetException;

public class RootContainerBuilder {

    UnitType unitType;
    UnitType.Version version;
    DataConverter dataConverter;

    public RootContainerBuilder(UnitType unitType, UnitType.Version version, DataConverter dataConverter) {
        checkVersion(unitType, version);
        this.unitType = unitType;
        this.version = version;
        this.dataConverter = dataConverter;
    }

    private void checkVersion(UnitType unitType, UnitType.Version version) {
        if (!unitType.hasVersion(version)) {
            throw new NoSuchVersionException(unitType, version);
        }
    }

    public XmlElementContainer build() throws IllegalAccessException, InstantiationException, ClassNotFoundException, NoSuchMethodException, InvocationTargetException {
        String elementName = unitType.getXmlRootElementNameForVersion(version);
        Object jaxbObjectFactory = getObjectFactory();
        JaxbReflectionHelper jaxbReflectionHelper = new JaxbReflectionHelper(jaxbObjectFactory);
        String packageName = jaxbObjectFactory.getClass().getPackage().getName();
        String className = packageName + "." + elementName;
        Object jaxbGeneratedObject = jaxbReflectionHelper.createObjectForJaxbClass(Class.forName(className));
        String xmlElementPath = elementName;
        return new XmlElementContainer(jaxbObjectFactory, jaxbGeneratedObject, xmlElementPath, dataConverter);
    }

    private Object getObjectFactory() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        String packageName = unitType.getPackageNameForVersion(version);
        String className = packageName + ".ObjectFactory";
        return Class.forName(className).newInstance();
    }
}
