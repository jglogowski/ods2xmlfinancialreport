/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.output.xml;

import pl.jg.ods2xmlfinancialreport.convert.DataConverter;
import pl.jg.ods2xmlfinancialreport.domain.UnitType;
import pl.jg.ods2xmlfinancialreport.output.Exporter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public class XmlExporter implements Exporter {

    private UnitType unitType;
    private UnitType.Version version;
    private DataConverter dataConverter;
    private File exportFile;


    public XmlExporter(File exportFile, UnitType unitType, UnitType.Version version, DataConverter dataConverter) {
        this.exportFile = exportFile;
        this.unitType = unitType;
        this.version = version;
        this.dataConverter = dataConverter;
    }

    @Override
    public void exportData() throws Exception {

        RootContainerBuilder rootContainerBuilder = new RootContainerBuilder(unitType, version, dataConverter);
        XmlElementContainer rootContainer = rootContainerBuilder.build();
        rootContainer.createChildren();

        String jaxbPackageName = rootContainer.getJaxbGeneratedPackage().getName();

        JAXBContext jc = JAXBContext.newInstance(jaxbPackageName);
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, unitType.getSchemaLocationForVersion(version));
        marshaller.marshal(rootContainer.getJaxbGeneratedObject(), exportFile);

    }
}
