/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport;

import pl.jg.ods2xmlfinancialreport.convert.DataConverter;
import pl.jg.ods2xmlfinancialreport.convert.DataConverterFactory;
import pl.jg.ods2xmlfinancialreport.convert.DataType;
import pl.jg.ods2xmlfinancialreport.domain.UnitType;
import pl.jg.ods2xmlfinancialreport.input.Importer;
import pl.jg.ods2xmlfinancialreport.input.attachment.FileImporter;
import pl.jg.ods2xmlfinancialreport.input.ods.OdsImporter;
import pl.jg.ods2xmlfinancialreport.output.Exporter;
import pl.jg.ods2xmlfinancialreport.output.Validator;
import pl.jg.ods2xmlfinancialreport.output.xml.XmlExporter;
import pl.jg.ods2xmlfinancialreport.output.xml.XmlValidator;

import javax.xml.datatype.DatatypeConfigurationException;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class Application {
    private final UnitType unitType;
    private final UnitType.Version unitVersion;
    private final Map<String, Object> rawData;
    private final DataConverter dataConverter;
    private final DataType dataTypeFrom;
    private final DataType dataTypeTo;
    private final String attachmentNameKey;
    private final String attachmentContentKey;
    private final String attachmentDescriptionKey;


    public Application() {
        unitType = UnitType.OTHER;
        unitVersion = UnitType.Version.V_1_0;
        rawData = new TreeMap<>();
        dataTypeFrom = DataType.ODS;
        dataTypeTo = DataType.XML;
        dataConverter = new DataConverterFactory().createConverter(dataTypeFrom, dataTypeTo, rawData);
        attachmentNameKey = unitType.getAttachedFileNameKeyForVersion(unitVersion);
        attachmentContentKey = unitType.getAttachedFileContentKeyForVersion(unitVersion);
        attachmentDescriptionKey = unitType.getAttachedFileDescriptionKeyForVersion(unitVersion);
    }

/*    public void run(String importFilePath, String exportFilePath, String[] infoFilePaths) {
        try {

            UnitType unitType = UnitType.OTHER;
            UnitType.Version unitVersion = UnitType.Version.V_1_0;

            File importFile = new File(importFilePath);
            //File infoFile = new File(infoFilePath);
            File[] infoFiles = ((File[]) Arrays.stream(infoFilePaths)
                    .map(path -> new File(path))
                    .toArray());
            File exportFile = new File(exportFilePath);

            Map<String, Object> rawData = new TreeMap<>();

            Importer importer = new OdsImporter(importFile, rawData);
            importer.importData();

            String attachmentNameKey = unitType.getAttachedFileNameKeyForVersion(unitVersion);
            String attachmentContentKey = unitType.getAttachedFileContentKeyForVersion(unitVersion);
            Importer attachmentImporter = new FileImporter(infoFiles, attachmentNameKey, attachmentContentKey, rawData);
            attachmentImporter.importData();

            DataConverter dataConverter = new DataConverterFactory().createConverter(DataType.ODS, DataType.XML, rawData);

            Exporter exporter = new XmlExporter(exportFile, unitType, unitVersion, dataConverter);
            exporter.exportData();


        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }*/

    public void runDataImport(String importFilePath) throws Exception {

            File importFile = new File(importFilePath);

            rawData.clear();

            Importer importer = new OdsImporter(importFile, rawData);
            importer.importData();
    }

    public List<String> getAttachmentsDescriptions() throws DatatypeConfigurationException {
        List<Object> descriptions = dataConverter.getConvertedToList(attachmentDescriptionKey, String.class,null);

        List<String> result = new ArrayList<>();

        if(descriptions != null && ! descriptions.isEmpty()) {
            result = dataConverter.getConvertedToList(attachmentDescriptionKey, String.class, null)
                    .stream()
                    .map(String.class::cast)
                    .collect(Collectors.toList());
        }

        return result;
    }

    public void runFileImport(String[] attachedFilesPaths) throws Exception {
            File[] attachedFiles = Arrays.stream(attachedFilesPaths)
                    .map(File::new)
                    .toArray(File[]::new);

            Importer attachmentImporter = new FileImporter(attachedFiles, attachmentNameKey, attachmentContentKey, rawData);
            attachmentImporter.importData();
    }

    public void runExport(String exportFilePath) throws Exception {
            File exportFile = new File(exportFilePath);
            Exporter exporter = new XmlExporter(exportFile, unitType, unitVersion, dataConverter);
            exporter.exportData();
    }

    public boolean runValidation(String xmlFilePath) throws Exception {
        File xmlFile = new File(xmlFilePath);
        Validator validator = new XmlValidator(xmlFile, unitType, unitVersion);
        return validator.validate();

    }

}
