/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.input.attachment;

import org.apache.commons.io.FileUtils;
import pl.jg.ods2xmlfinancialreport.input.Importer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

public class FileImporter implements Importer {

    private File[] attachedFiles;
    private String dataContainerNameKey;
    private String dataContainerContentKey;
    private Map<String, Object> rawData;

    public FileImporter(File[] attachedFiles, String dataContainerNameKey, String dataContainerContentKey, Map<String, Object> rawData) {
        this.attachedFiles = attachedFiles;
        this.dataContainerNameKey = dataContainerNameKey;
        this.dataContainerContentKey = dataContainerContentKey;
        this.rawData = rawData;
    }

    @Override
    public void importData() throws IOException {

        List<String> fileNames = new ArrayList<>();
        List<byte[]> fileContents = new ArrayList<>();

        for (int i = 0; i < attachedFiles.length; i++) {
            fileNames.add(attachedFiles[i].getName());
            byte[] fileContent = FileUtils.readFileToByteArray(attachedFiles[i]);
            byte[] fileContentBase64Encoded = Base64.getEncoder().encodeToString(fileContent).getBytes();
            fileContents.add(fileContentBase64Encoded);
        }


        rawData.put(dataContainerNameKey, fileNames);
        rawData.put(dataContainerContentKey, fileContents);

    }
}
