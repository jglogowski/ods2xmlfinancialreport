/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.input.ods;

import org.jdom.Element;
import org.jdom.Namespace;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OdsNamedElementsExtractor {
    private final SpreadSheet spreadSheet;
    private Map<String, OdsNamedElementWrapper> namedElements;
    private final Namespace tableNS = Namespace.getNamespace("table", "urn:oasis:names:tc:opendocument:xmlns:table:1.0");
    private final Namespace officeNS = Namespace.getNamespace("office", "urn:oasis:names:tc:opendocument:xmlns:office:1.0");
    private final Namespace formNS = Namespace.getNamespace("form", "urn:oasis:names:tc:opendocument:xmlns:form:1.0");

    public OdsNamedElementsExtractor(SpreadSheet spreadSheet) throws IOException {
        this(spreadSheet, null);
    }

    public OdsNamedElementsExtractor(SpreadSheet spreadSheet, Map<String, OdsNamedElementWrapper> namedElements) throws IOException {
        this.spreadSheet = spreadSheet;
        this.namedElements = namedElements == null ? new HashMap<>() : namedElements;
    }

    public Map<String, OdsNamedElementWrapper> extract() {
        namedElements.clear();
        extractNamedCellRanges();
        extractFormControls();
        return namedElements;
    }

    private void extractNamedCellRanges() {


        Element namedExpressionsElement = (Element)
                spreadSheet
                        .getPackage()
                        .getContentType()
                        .getBody(spreadSheet.getContentDocument())
                        .getChildren("named-expressions", tableNS)
                        .get(0);

        List<Element> namedRangesList = namedExpressionsElement.getChildren("named-range", tableNS);

        for (Element namedRange : namedRangesList) {
            String name = namedRange.getAttributeValue("name", tableNS);
            String rangeAddress = namedRange.getAttributeValue("cell-range-address", tableNS);

            OdsNamedElementWrapper element = null;

            if (rangeAddress.matches(".*:.*")) { // cell range
                element = new OdsNamedCellRangeWrapper(name, spreadSheet, rangeAddress);
            } else { // single cell
                element = new OdsNamedCellWrapper(name, spreadSheet, rangeAddress);
            }

            namedElements.put(name, element);
        }

    }


    private void extractFormControls() {

        for (int i = 0; i < spreadSheet.getSheetCount(); i++) {
            Sheet sheet = spreadSheet.getSheet(i);
            List<Element> formsElementsList = sheet.getElement().getChildren("forms", officeNS);
            for (Element formsElement : formsElementsList) {
                List<Element> formElementsList = formsElement.getChildren("form", formNS);
                for (Element form : formElementsList) {
                    for (OdsFormControlType controlType : OdsFormControlType.values()) {
                        List<Element> formControls = form.getChildren(controlType.getElementName(), formNS);
                        for (Element formControl : formControls) {
                            extractFormControl(controlType, formControl);
                        }
                    }
                }
            }
        }
    }

    private void extractFormControl(OdsFormControlType controlType, Element formControl) {

        String name = formControl.getAttributeValue("name", formNS);
        OdsNamedElementWrapper elementWrapper = null;

        switch (controlType) {
            case TEXTAREA:
                elementWrapper = new OdsTextAreaWrapper(name, formControl);
                break;
            case CHECKBOX:
                elementWrapper = new OdsCheckBoxWrapper(name, formControl);
                break;
            case RADIOBUTTON:
                if (formControl.getAttributeValue("current-selected", formNS, "false").equals("true")) {
                    elementWrapper = new OdsRadioButtonWrapper(name, formControl);
                }
                break;
        }

        if (elementWrapper != null) {
            namedElements.put(name, elementWrapper);
        }
    }
/*

    private void extractTextAreas() {

        for (int i = 0; i < spreadSheet.getSheetCount(); i++) {
            Sheet sheet = spreadSheet.getSheet(i);
            List<Element> formsElementsList = sheet.getElement().getChildren("forms", ns1);
            for (Element formsElement : formsElementsList) {
                List<Element> formElementsList = formsElement.getChildren("form", ns2);
                for (Element form : formElementsList) {
                    List<Element> textAreas = form.getChildren("textarea", ns2);
                    for (Element textArea : textAreas) {
                        String name = textArea.getAttributeValue("name", ns2);
                        namedElements.put(name, new OdsTextAreaWrapper(name, textArea));
                    }
                }
            }
        }
    }
*/

/*
    private void extractRadioButtons() {
        Namespace ns1 = Namespace.getNamespace("office", "urn:oasis:names:tc:opendocument:xmlns:office:1.0");
        Namespace ns2 = Namespace.getNamespace("form", "urn:oasis:names:tc:opendocument:xmlns:form:1.0");

        for (int i = 0; i < spreadSheet.getSheetCount(); i++) {
            Sheet sheet = spreadSheet.getSheet(i);
            List<Element> formsElementsList = sheet.getElement().getChildren("forms", ns1);
            for (Element formsElement : formsElementsList) {
                List<Element> formElementsList = formsElement.getChildren("form", ns2);
                for (Element form : formElementsList) {
                    List<Element> radioButtons = form.getChildren("radio", ns2);
                    for (Element radioButton : radioButtons) {
                        if (radioButton.getAttributeValue("current-selected", ns2, "false").equals("true")) {
                            String name = radioButton.getAttributeValue("name", ns2);
                            namedElements.put(name, new OdsRadioButtonWrapper(name, radioButton));
                        }
                    }
                }
            }
        }
    }
*/

/*
    private void extractCheckBoxes() {

        Namespace ns1 = Namespace.getNamespace("office", "urn:oasis:names:tc:opendocument:xmlns:office:1.0");
        Namespace ns2 = Namespace.getNamespace("form", "urn:oasis:names:tc:opendocument:xmlns:form:1.0");

        for (int i = 0; i < spreadSheet.getSheetCount(); i++) {
            Sheet sheet = spreadSheet.getSheet(i);
            List<Element> formsElementsList = sheet.getElement().getChildren("forms", ns1);
            for (Element formsElement : formsElementsList) {
                List<Element> formElementsList = formsElement.getChildren("form", ns2);
                for (Element form : formElementsList) {
                    List<Element> checkboxes = form.getChildren("checkbox", ns2);
                    for (Element checkbox : checkboxes) {
                        String name = checkbox.getAttributeValue("name", ns2);
                        namedElements.put(name, new OdsCheckBoxWrapper(name, checkbox));
                    }
                }
            }
        }
    }
*/

}
