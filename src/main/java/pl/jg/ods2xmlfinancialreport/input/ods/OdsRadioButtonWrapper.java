/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.input.ods;

import org.jdom.Element;
import org.jdom.Namespace;

public class OdsRadioButtonWrapper implements OdsNamedElementWrapper {
    private final String name;
    private final Element radioButton;
    private final Namespace ns;

    public OdsRadioButtonWrapper(String name, Element radioButton) {
        this.name = name;
        this.radioButton = radioButton;
        this.ns = radioButton.getNamespace();

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Object getValue() {
        if (! isEmpty()) {
            return radioButton.getAttributeValue("value", ns);
        }

        return null;
    }

    @Override
    public void setValue(Object valueObject) {

        String fixedValue = radioButton.getAttributeValue("value", ns);

        if (fixedValue == null) {
            return;
        }

        String newValue = (String) valueObject;

        if (fixedValue.equals(newValue)) {
            radioButton.setAttribute("current-selected", "true", ns);
        } else {
            radioButton.removeAttribute("current-selected", ns);
        }

    }

    @Override
    public boolean isEmpty() {
        return  ! radioButton.getAttributeValue("current-selected", ns, "false").equals("true");
    }
}
