/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.input.ods;

import org.jopendocument.dom.spreadsheet.MutableCell;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

public class OdsNamedCellWrapper implements OdsNamedElementWrapper {

    private final String name;
    private final MutableCell cell;

    public OdsNamedCellWrapper(String name, SpreadSheet spreadSheet, String address) {
        this.name = name;
        Sheet sheet= OdsUtils.getSheet(spreadSheet, address);
        String cellAddress = OdsUtils.getCellAddressPart(spreadSheet,address);
        this.cell = sheet.getCellAt(cellAddress);
    }

    public OdsNamedCellWrapper(String name, SpreadSheet spreadSheet, String sheetName, int rowNr, int colNr) {
        this.name = name;
        this.cell = spreadSheet.getSheet(sheetName).getCellAt(rowNr, colNr);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Object getValue() {
        return cell.getValue();
    }

    @Override
    public void setValue(Object valueObject) {
        cell.setValue(valueObject);
    }

    @Override
    public boolean isEmpty() {
        return cell.isEmpty();
    }

}
