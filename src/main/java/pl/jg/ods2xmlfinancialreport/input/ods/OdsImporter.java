/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.input.ods;

import org.jopendocument.dom.spreadsheet.*;
import pl.jg.ods2xmlfinancialreport.input.Importer;

import java.io.File;
import java.io.IOException;

import java.util.Map;

public class OdsImporter implements Importer {
    Map<String, Object> rawData;
    SpreadSheet spreadSheet;

    public OdsImporter(File odsFile, Map<String, Object> rawData) throws IOException {
        this.spreadSheet = SpreadSheet.createFromFile(odsFile);
        this.rawData = rawData;
    }

    @Override
    public void importData() throws IOException {
        OdsNamedElementsExtractor namedElementsExtractor = new OdsNamedElementsExtractor(spreadSheet);
        Map<String, OdsNamedElementWrapper> namedElements = namedElementsExtractor.extract();
        namedElements.entrySet().forEach(e -> rawData.put(e.getKey(), e.getValue().getValue()));
    }

}
