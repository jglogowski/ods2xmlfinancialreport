/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.input.ods;

import org.jopendocument.dom.spreadsheet.SpreadSheet;

import java.util.List;
import java.util.stream.Collectors;

public class OdsNamedCellRangeWrapper implements OdsNamedElementWrapper {
    private final String name;
    private final SpreadSheet spreadSheet;
    private final List<OdsNamedCellWrapper> cells;


    public OdsNamedCellRangeWrapper(String name, SpreadSheet spreadSheet, String rangeAddress) {
        this.name = name;
        this.spreadSheet = spreadSheet;
        this.cells = getCells(rangeAddress);

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Object getValue() {
        return cells.stream()
                .filter(cell -> !cell.isEmpty())
                .map(cell -> cell.getValue())
                .collect(Collectors.toList());

    }

    @Override
    public void setValue(Object valueObject) {
        if (!(valueObject instanceof List)) {
            return;
        }

        int count = Integer.min(((List) valueObject).size(), cells.size());

        for (int i = 0; i < count; i++) {
            cells.get(i).setValue(((List) valueObject).get(i));
        }
    }

    @Override
    public boolean isEmpty() {
        return cells.stream()
                .anyMatch(cell -> !cell.isEmpty());
    }


    private List<OdsNamedCellWrapper> getCells(String rangeAddress) {
        return OdsUtils.getAbsoluteAddressesForRange(spreadSheet, rangeAddress).stream()
                .map(addr -> new OdsNamedCellWrapper(name, spreadSheet, addr))
                .collect(Collectors.toList());
    }


}
