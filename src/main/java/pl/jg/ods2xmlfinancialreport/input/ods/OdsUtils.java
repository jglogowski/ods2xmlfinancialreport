/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.input.ods;

import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;

import java.util.ArrayList;
import java.util.List;

public class OdsUtils {
    public static int getRowNumber(SpreadSheet spreadSheet, String cellAddress) {
        return (int) spreadSheet.resolve(cellAddress).get1().getY();
    }

    public static int getColumnNumber(SpreadSheet spreadSheet, String cellAddress) {
        return (int) spreadSheet.resolve(cellAddress).get1().getX();
    }

    public static Sheet getSheet(SpreadSheet spreadSheet, String cellAddress) {
        return spreadSheet.resolve(cellAddress).get0();
    }

    public static String getCellAddressPart(SpreadSheet spreadSheet, String cellAddress) {
        int rowNr = getRowNumber(spreadSheet, cellAddress);
        int colNr = getColumnNumber(spreadSheet, cellAddress);
        return "$" + getColumnString(colNr) + "$" + getRowString(rowNr);
    }

    public static String getSheetName(SpreadSheet spreadSheet, String cellAddress) {
        return getSheet(spreadSheet, cellAddress).getName();
    }

    public static String getColumnString(int colNr) {
        return getColumnString(colNr, 0);
    }

    private static String getColumnString(int colNr, int recurrencyLevel) {

        if (colNr < 0) {
            return null;
        }

        int aValue = (int) 'A';
        int zValue = (int) 'Z';

        int lettersCount = zValue - aValue + 1;

        if (colNr < lettersCount) {
            return String.valueOf((char) (aValue + colNr));
        } else {
            return getColumnString(aValue + recurrencyLevel, recurrencyLevel + 1)
                    + getColumnString(colNr - lettersCount);
        }
    }

    public static String getRowString(int rowNr) {
        return String.valueOf(rowNr + 1);
    }

    public static String getAbsoluteAddress(String sheetName, int rowNr, int colNr) {
        return "$'" + sheetName + "'.$" + getColumnString(colNr) + "$" + getRowString(rowNr);
    }


    //  Range adresses in form $Wprowadzenie.$B$15:.$F$15 assumed
    public static List<String> getAbsoluteAddressesForRange(SpreadSheet spreadSheet, String rangeAddress) {

        List<String> addresses = new ArrayList<>();

        String[] rangeAdressParts = rangeAddress.split(":");
        String startAdress = rangeAdressParts[0];

        String sheetName = getSheetName(spreadSheet, startAdress);
        int startX = getRowNumber(spreadSheet, startAdress);
        int startY = getColumnNumber(spreadSheet, startAdress);

        String endAdress = rangeAdressParts[1];
        String[] endAdressParts = endAdress.split("\\.");
        String endCellAdress = endAdressParts[1];
        endAdress = "$'" + sheetName + "'." + endCellAdress;
        int endX = getRowNumber(spreadSheet, endAdress);
        int endY = getColumnNumber(spreadSheet, endAdress);

        for (int i = startX; i <= endX; i++) {
            for (int j = startY; j <= endY; j++) {
                addresses.add(getAbsoluteAddress(sheetName, i, j));
            }
        }

        return addresses;
    }
}
