/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.input.ods;

import org.jdom.Element;
import org.jdom.Namespace;

public class OdsCheckBoxWrapper implements OdsNamedElementWrapper {

    private final Element checkbox;
    private final String name;
    private final Namespace ns;

    public OdsCheckBoxWrapper(String name, Element checkbox) {
        this.name = name;
        this.checkbox = checkbox;
        ns = checkbox.getNamespace();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Object getValue() {
        return new Boolean(!isEmpty());
    }

    @Override
    public void setValue(Object valueObject) {
        Boolean newValue = (Boolean) valueObject;
        if (newValue) {
            checkbox.setAttribute("current-state", "checked", ns);
            checkbox.setAttribute("value", "true", ns);
        } else {
            checkbox.removeAttribute("current-state", ns);
            checkbox.removeAttribute("value", ns);
        }
    }

    @Override
    public boolean isEmpty() {

        boolean checked = checkbox.getAttributeValue("current-state", ns, "anything").equals("checked");
        boolean valueIsTrue = checkbox.getAttributeValue("value", ns, "whatever").equals("true");

        return !(checked && valueIsTrue);
    }
}
