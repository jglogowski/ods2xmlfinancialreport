/*
 * e-sprawozdania finansowe ODS - converting Open Office spreadsheets to financial reports in XML-format required by Polish Ministry of Finance
 * Copyright (C) 2019 Jarosław Głogowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


package pl.jg.ods2xmlfinancialreport.input.ods;

import org.jdom.Element;
import org.jdom.Namespace;

public class OdsTextAreaWrapper implements OdsNamedElementWrapper {
    private final String name;
    private final Element textArea;
    private final Namespace ns;

    public OdsTextAreaWrapper(String name, Element textArea) {
        this.name = name;
        this.textArea = textArea;
        this.ns = textArea.getNamespace();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Object getValue() {
        return textArea.getAttributeValue("current-value", ns);
    }

    @Override
    public void setValue(Object valueObject) {
        if (valueObject == null || valueObject.toString().isEmpty()) {
            textArea.removeAttribute("current-value", ns);
        } else {
            textArea.setAttribute("current-value", valueObject.toString(), ns);
        }

    }

    @Override
    public boolean isEmpty() {
        Object value = getValue();
        return value == null || value.toString().isEmpty();
    }
}
