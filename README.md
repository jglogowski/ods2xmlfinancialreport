# e-Sprawozdanie-ODS
## O programie
`e-Sprawozdanie-ODS` to niewielkie, graficzne narzędzie do generowania sprawozdań finansowych w formacie XML, zgodnym ze schematem opublikowanym przez Ministerstwo Finansów. Format ten jest od 1 października 2018 roku jedynym obowiązującym dla wielu polskich podmiotów gospodarczych , składających doroczne sprawozdania do Krajowego Rejestru Sądowego lub Krajowej Administracji Skarbowej.

Aby wygenerować sprawozdanie, należy najpierw wypełnić i zapisać arkusz Open Office na podstawie szablonu udostępnionego razem z programem.

## Wymagania
Do uruchomienia pliku `.jar` wymagane jest Java Runtime Environment w wersji co najmniej 1.8.0_201. Wersja dla Windows zawiera wbudowaną, prywatną dystrybucję środowiska Javy, w związku z tym jego oddzielna instalacja nie jest konieczna.

## Jak używać
1. Wypełnić i zapisać arkusz Open Office utworzony z szablonu `e-sprawozdanie-jednostka-inna-v1-0.ots`:    ![e-sprawozdanie-ODS - arkusz Open Office](https://bitbucket.org/jglogowski/ods2xmlfinancialreport/raw/5f01467a28a25911bbe0ed48efaad89222cedfc1/screens/screen1.jpg)
2. Uruchomić program konwertujący i przejść przez kolejne kroki (w tym dołączenie załączników z informacją dodatkową):    ![e-sprawozdanie-ODS - konwerter](https://bitbucket.org/jglogowski/ods2xmlfinancialreport/raw/964c7c7111e84700334a44328dff79244beca281/screens/screen2.jpg)
3. Wygenerowany plik XML można podejrzeć w przeglądarce internetowej.    ![e-sprawozdanie-ODS - wygenerowany plik XML](https://bitbucket.org/jglogowski/ods2xmlfinancialreport/raw/964c7c7111e84700334a44328dff79244beca281/screens/screen3.jpg)

## Licencja

Niniejszy program jest wolnym oprogramowaniem - możesz go rozpowszechniać dalej i/lub modyfikować na warunkach [Powszechnej Licencji Publicznej GNU (GNU GPL)](http://www.gnu.org/licenses/gpl.html) wydanej przez [Free Software Foundation](https://www.fsf.org/), według wersji 3 tej Licencji lub dowolnej z późniejszych wersji. Niniejszy program rozpowszechniany jest z nadzieją, iż będzie on użyteczny - jednak BEZ ŻADNEJ GWARANCJI, nawet domyślnej gwarancji PRZYDATNOŚCI HANDLOWEJ albo PRZYDATNOŚCI DO OKREŚLONYCH ZASTOSOWAŃ. Bliższe informacje na ten temat można uzyskać z Powszechnej Licencji Publicznej GNU.

W szczególności autor, nie pobierając żadnych opłat z tytułu rozpowszechniania tego programu, nie ponosi odpowiedzialności za jakiekolwiek skutki jego użytkowania.

Icon made by [Freepik](https://www.freepik.com/) from [www.flaticon.com](https://www.flaticon.com/) is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)

## Download
Pliki wykonywalne programu, szablon Open Document Spreadsheet oraz pliki przykładowe można pobrać w sekcji [Downloads](https://bitbucket.org/jglogowski/ods2xmlfinancialreport/downloads/).

## Sponsor
Program powstał dzięki krakowskiemu [Biuru Rachunkowości Komputerowej i Doradztwa Podatkowego **REJESTR**](http://www.biuro-rejestr.krakow.pl).

## Autor
Jarosław Głogowski

[e-sprawozdania@sklikow.pl](mailto:e-sprawozdania@sklikow.pl)

